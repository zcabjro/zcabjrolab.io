const path = require('path')
const { createFilePath } = require('gatsby-source-filesystem')

exports.onCreateNode = ({ node, getNode, boundActionCreators }) => {
  const { createNodeField } = boundActionCreators
  if (node.internal.type === 'MarkdownRemark') {
    const slug = createFilePath({ node, getNode, basePath: 'pages' })
    createNodeField({
      node,
      name: 'slug',
      value: slug,
    })
    const category = path.basename(path.dirname(node.fileAbsolutePath))
    createNodeField({
      node,
      name: 'category',
      value: category
    })
  }
}

exports.createPages = ({ graphql, boundActionCreators }) => {
  const { createPage } = boundActionCreators
  return new Promise((resolve, reject) => {
    graphql(`
      {
        allMarkdownRemark {
          edges {
            node {
              fields {
                slug
                category
              }
            }
          }
        }
      }
    `
    ).then(result => {
      result.data.allMarkdownRemark.edges.map(({ node }) => {
        let template = null
        switch (node.fields.category) {
          case 'about':
            template = './src/templates/about.js'
            break
          case 'post':
            template = './src/templates/post.js'
            break
          case 'project':
            template = './src/templates/project.js'
            break
        }

        if (!template) {
          reject(new Error('Unhandled page category: ' + node.fields.category))
        }

        createPage({
          path: node.fields.slug,
          component: path.resolve(template),
          context: {
            // Data passed to context is available in page queries as GraphQL variables.
            slug: node.fields.slug,
            category: node.fields.category
          },
        })
      })
      resolve()
    })
  })
}
