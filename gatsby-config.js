module.exports = {
  siteMetadata: {
    title: 'Jack Roper',
  },
  plugins: [{
    resolve: 'gatsby-plugin-react-helmet'
  }, {
    resolve: 'gatsby-source-filesystem',
    options: {
      name: 'src',
      path: `${__dirname}/src/`
    },
  }, {
    resolve: 'gatsby-transformer-sharp'
  }, {
    resolve: 'gatsby-transformer-remark',
    options: {
      plugins: [{
        resolve: 'gatsby-remark-images',
        options: {
          maxWidth: 600,
          linkImagesToOriginal: true
        }
      }]
    }
  }]
}
