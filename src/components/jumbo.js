import React from 'react'
import Flex from './flex.js'
import Divider from './divider.js'

export default props =>
  <Flex col hc style={{ height: '100%', paddingTop: '1rem', paddingBottom: '1rem' }}>
    <Flex col hc vc style={{ height: '100%' }}>
      {props.children}
    </Flex>
  </Flex>