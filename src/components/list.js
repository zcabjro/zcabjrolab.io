import React from 'react'
import Flex from './flex.js'
import Divider from './divider.js'

export default props =>
  <Flex col>
    {React.Children.map(props.children, (child, i) => {
      // Wrap child in a flex with margin
      child = <Flex style={{ margin: '1rem' }}>{child}</Flex>
      // Add divider between children if divide was specified
      if (props.divide) {
        return i === 0 ? child : [<Divider/>, child]
      }
      return child
    })}
  </Flex>