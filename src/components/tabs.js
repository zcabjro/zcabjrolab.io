import React from 'react'
import Flex from './flex.js'

export default class Tabs extends React.Component {
  constructor () {
    super()
    this.state = {
      selected: 0
    }
    this.handleClick = this.handleClick.bind(this)
  }

  handleClick (tabIndex) {
    this.setState({ selected: tabIndex })
  }

  render () {
    const tabs = this.props.tabs && this.props.tabs.map((tab, index) =>
      <h3 key={index}
        style={{ marginLeft: '1rem', margin: '0' }}
        onClick={() => this.handleClick(index)}
      >{tab}</h3>
    )

    const children = React.Children.toArray(this.props.children)

    return (
      <Flex col>
        <Flex>{tabs}</Flex>
        <Flex>{children[this.state.selected]}</Flex>
      </Flex>
    )
  }
}