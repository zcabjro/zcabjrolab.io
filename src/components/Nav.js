import React from 'react'
import Flex from './flex.js'
import NavLink from './nav-link.js'
import FontAwesome from 'react-fontawesome'

const Nav = props =>
  <Flex hr style={{ fontSize: '1.5em' }}>
    <header>
      <ul style={{ listStyle: 'none', margin: '0px', paddingLeft: '1rem' }}>
        <NavLink to='/'>Home</NavLink>
        <NavLink to='/about/'>About</NavLink>
        <NavLink abs to='https://www.linkedin.com/in/jackhroper/'>
          <FontAwesome name='linkedin'/>
        </NavLink>
      </ul>
    </header>
  </Flex>

export default Nav