import React from 'react'

export default props =>
  <hr style={{
    width: '100%',
    borderColor: '#ffffff4d',
    marginTop: '1rem',
    marginBottom: '1rem'
  }}/>