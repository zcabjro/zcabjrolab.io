import React from 'react'
import { calculateWidth } from '../utils/media-query'

class Flex extends React.Component {
  constructor () {
    super()
    this.state = {
      width: 0
    }
    this.handleResize = this.handleResize.bind(this)
  }

  componentDidMount () {
    window.addEventListener('resize', this.handleResize)
    this.handleResize()
  }

  componentWillUnmount () {
    window.removeEventListener('resize', this.handleResize)
  }

  handleResize () {
    const width = calculateWidth(this.props.xs, this.props.sm, this.props.md, this.props.lg, this.props.xl)
    this.setState({ width })
  }

  render ()  {
    let flexDirection = 'row'
    let horizontal = 'justifyContent'
    let vertical = 'alignItems'
    if (this.props.col) {
      flexDirection = 'column'
      let tmp = horizontal
      horizontal = vertical
      vertical = tmp
    }

    let style = {
      display: 'flex',
      flexWrap: this.props.wrap ? 'wrap' : 'no-wrap',
      width: this.state.width,
      flexDirection
    }

    style[horizontal] = this.props.hr ? 'flex-end' : this.props.hc ? 'center' : 'flex-start'
    style[vertical] = this.props.vr ? 'flex-end' : this.props.vc ? 'center' : 'flex-start'

    // Assign consumer-supplied style
    if (this.props.style) {
      style = Object.assign(style, this.props.style)
    }

    const marginLeft = style.marginLeft || style.margin
    const marginRight = style.marginRight || style.margin
    const paddingLeft = style.paddingLeft || style.padding
    const paddingRight = style.paddingRight || style.padding

    let width = 'calc(' + this.state.width
    if (marginLeft) width += ' - ' + marginLeft
    if (marginRight) width += ' - ' + marginRight
    if (paddingLeft) width += ' - ' + paddingLeft
    if (paddingRight) width += ' - ' + paddingRight
    width += ')'

    style.width = width

    return (
      <div style={style}>
        {this.props.children}
      </div>
    )
  }
}

export default Flex