import React from 'react'

export default class Highlight extends React.Component {
  constructor () {
    super()
    this.state = {
      highlight: false
    }
    this.handleMouseOver = this.handleMouseOver.bind(this)
    this.handleMouseOut = this.handleMouseOut.bind(this)
  }

  handleMouseOver () {
    this.setState({ highlight: true })
  }

  handleMouseOut () {
    this.setState({ highlight: false })
  }

  render () {
    let style = {
      width: '100%',
      height: '100%',
      boxShadow: '-2px 2px ' + (this.state.highlight ? 15 : 5) + 'px 0px rgba(189,189,189,0.3)'
    }

    return (
      <div style={style} onMouseOver={this.handleMouseOver} onMouseOut={this.handleMouseOut}>
        {this.props.children}
      </div>
    )
  }
}