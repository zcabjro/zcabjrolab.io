import React from 'react'

export default props =>
  <div style={{ width: '100%', paddingTop: '56.25%', position: 'relative' }}>
    <iframe
      style={{ position: 'absolute', 'left': 0, 'top': 0, width: '100%', height: '100%'}}
      src={props.src}
      frameBorder="0"
      allowFullScreen
    ></iframe>
  </div>