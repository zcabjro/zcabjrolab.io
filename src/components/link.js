import React from 'react'
import Link from 'gatsby-link'

export default props => {
  const style = { textDecoration: 'none' }
  return props.abs
    ? <a href={props.to} target='_blank' style={style}>{props.children}</a>
    : <Link to={props.to} style={style}>{props.children}</Link>
}