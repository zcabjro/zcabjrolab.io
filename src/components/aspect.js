import React from 'react'

export default props =>
  <div style={{
    paddingTop: '100%',
    width: '100%',
    display: 'inline-block',
    position: 'relative'
  }}>
    <div style={{
      position: 'absolute',
      top: '0',
      bottom: '0',
      left: '0',
      right: '0'
    }}>
      {props.children}
    </div>
  </div>
