import React from 'react'
import Nav from '../components/nav.js'
import Flex from '../components/flex.js'
import Helmet from 'react-helmet'
import favicon from '../assets/favicon.ico'
import './index.css'

export default ({ children, data }) =>
  <div>
    <Helmet>
      <meta charSet="utf-8" />
      <title>{data.site.siteMetadata.title}</title>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
      <link href="https://fonts.googleapis.com/css?family=Oxygen" rel="stylesheet"/>
      <link rel="icon" href={favicon} type="image/x-icon" />
    </Helmet>

    <div style={{ padding: '1rem' }}>
      <Flex hc>
        <Flex wrap hc xs={12} md={8}>
          <Nav/>
          <Flex col hc>
            {children()}
          </Flex>
        </Flex>
      </Flex>
    </div>
  </div>

export const query = graphql`
  query Title {
    site {
      siteMetadata {
        title
      }
    }
  }
`
