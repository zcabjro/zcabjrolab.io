function getBreakpoint () {
  if (window.matchMedia('(max-width: 600px)').matches) {
    return 0
  } else if (window.matchMedia('(max-width: 960px)').matches) {
    return 1
  } else if (window.matchMedia('(max-width: 1264px').matches) {
    return 2
  } else if (window.matchMedia('(max-width: 1904px)').matches) {
    return 3
  } else {
    return 4
  }
}

function getColumns (xs, sm, md, lg, xl) {
  // Fall-through size cases
  switch (getBreakpoint()) {
    case 4:
      if (xl !== undefined) return xl
    case 3:
      if (lg !== undefined) return lg
    case 2:
      if (md !== undefined) return md
    case 1:
      if (sm !== undefined) return sm
    default:
      return xs !== undefined ? xs : 12
  }
}

export function calculateWidth (xs, sm, md, lg, xl) {
  const cols = getColumns(xs, sm, md, lg, xl)
  return (100 * (cols / 12.0)) + '%'
}