import React from 'react'
import moment from 'moment'
import List from '../components/list.js'
import Highlight from '../components/highlight.js'
import Link from '../components/link.js'
import Flex from '../components/flex.js'
import Jumbo from '../components/jumbo.js'
import Tabs from '../components/tabs.js'

export default ({ data }) => {
  const dateOrderedMarkdown = data.allMarkdownRemark.edges
    .filter(({ node }) => node.frontmatter.date)
    .sort(function (a, b) {
      const d1 = moment(a.node.frontmatter.date)
      const d2 = moment(b.node.frontmatter.date)
      if (d1 < d2) return 1
      if (d1 > d2) return -1
      return 0
    })

  const latest = dateOrderedMarkdown
  const posts = dateOrderedMarkdown.filter(({ node }) => node.fields.category === 'post')
  const projects = dateOrderedMarkdown.filter(({ node }) => node.fields.category === 'project')

  return (
    <Flex col>
      <Flex style={{ minHeight: '128px', height: '20vh' }}>
        <Jumbo>
          <h1>Jack Roper</h1>
        </Jumbo>
      </Flex>
      <h2>Latest Posts Projects</h2>
      <Tabs tabs={['Latest', 'Posts', 'Projects']}>
        <h1>Latest stuff</h1>
        <h1>Posts stuff</h1>
        <h1>Projects stuff</h1>
      </Tabs>
      <List>
        {latest.map(({ node }) => 
          <Highlight key={node.id}>
            <Link to={node.fields.slug}>
              <Flex col style={{ padding: '1rem' }}>
                <Flex xs={12}>
                  <h3 style={{ margin: '0' }}>
                    {node.frontmatter.title}
                  </h3>
                  <span style={{ marginLeft: '1rem' }}>
                    &ndash;
                    {moment(node.frontmatter.date).format(' MMMM YYYY')}
                  </span>
                </Flex>
                <Flex>
                  <p>{node.excerpt}</p>
                </Flex>
              </Flex>
            </Link>
          </Highlight>
        )}
      </List>
    </Flex>
  )
}

export const query = graphql`
  query IndexPageQuery {
    allMarkdownRemark {
      edges {
        node {
          id
          frontmatter {
            title
            date
          }
          excerpt
          fields {
            slug
            category
          }
        }
      }
    }
  }
`