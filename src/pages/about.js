import React from 'react'
import Flex from '../components/flex.js'
import Link from '../components/link.js'
import List from '../components/list.js'
import Highlight from '../components/highlight.js'
import Jumbo from '../components/jumbo.js'

export default props => {
  const abouts = props.data.allMarkdownRemark.edges
    .filter(({ node }) => node.fields.category === 'about')
    .sort(function (a, b) {
      const t1 = a.node.frontmatter.title
      const t2 = b.node.frontmatter.title
      if (t1 > t2) return 1
      if (t1 < t2) return -1
      return 0
    })
  return (
    <Flex col hc>
      <Flex style={{ minHeight: '128px', height: '20vh' }}>
        <Jumbo>
          <h1>About me</h1>
        </Jumbo>
      </Flex>
      <Flex xs={12} md={8}>
        <List>
          {abouts.map(({ node }) => {
            return (
              <Highlight key={node.id}>
                <Link to={node.fields.slug}>
                  <Flex style={{ padding: '1rem' }}>
                    <h2>{node.frontmatter.title}</h2>
                  </Flex>
                </Link>
              </Highlight>
            )
          })}
        </List>
      </Flex>
    </Flex>
  )
}

export const query = graphql`
query AboutPageQuery {
  allMarkdownRemark {
    edges {
      node {
        id
        frontmatter {
          title
        }
        fields {
          slug
          category
        }
      }
    }
  }
}
`