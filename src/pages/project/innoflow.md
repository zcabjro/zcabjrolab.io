---
title: "InnoFlow"
date: "2017-03"
youtube: "https://www.youtube-nocookie.com/embed/i6VebdBypT8?rel=0&amp;showinfo=0"
---

Universities and training providers are faced with the challenge of overseeing many, often highly technical, projects at a time. As it stands, student software engineering projects are largely opaque in that key updates and progression information rarely make it past the students directly involved. Individual students’ contributions are equally isolated across multiple projects, making it difficult for educational entities and employers to appraise students effectively. Assessment will typically take place at the end where probing a project for qualities that should have been observable from the start proves most difficult. This poses a problem for project management and assessment, where human resources are limited while pressure on students and staff to perform well is comparatively high.

To help address this disparity, we developed an software platform called InnoFlow where project development behaviour is made largely transparent to supervisors and where feedback and assessment are made top priorities. Previous work has largely focused on automating assessment of programming exercises and peer review. We extended this approach to more open-ended projects, integrating it with the peer review process to further reduce the burden on teaching staff. Moreover, projects are grouped into classes and their source-control repositories analysed to produce quantifiable metrics as a means of assessment and self-improvement.

## Key Features

* Passive assessment through tracked commit activity, code review and peer-feedback actions
* Innovation showcasing through the code snippet and markdown REST API, with VS Code integration
* Extensibility demonstrated by sample [Chrome extension](https://github.com/zcabjro/InnoFlowChrome)
* Decoupled Single-page app (Vue.js), RESTful API and database (Laravel) and VS Code extension

## Contribution

As the team's leader, I was responsible for overseeing development activity and prioritising tasks on a weekly basis. To facilitate this, I set up and managed the project repositories and task boards (via Trello), as well as taking charge of weekly supervisor meetings and client liaison. While the team separated into groups during early development, working on separate components such as the InnoFlow API and VS Code extension, I remained a part of each process in the interest of their continuous integration.

I was also primarily responsible for developing InnoFlow's frontend. This includes authoring all the Vue.js components used to weave together the interface and business logic in a component-oriented manner.