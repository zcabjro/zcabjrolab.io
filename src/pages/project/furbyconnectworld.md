---
title: "Furby Connect World"
date: "2016-06"
youtube: https://www.youtube-nocookie.com/embed/DF78RiBSi-M?rel=0&amp;showinfo=0"
---

During a 13 week summer internship at Exient Oxford, I worked alongside industry professionals on the Hasbro licensed Furby Connect World mobile game. This primarily involved working in C++ to iteratively extend and improve game's feature set using an agile methodology. The experience presented a great opportunity to become familiar with a large, existing codebase, offering many enjoyable challenges to overcome with resepect to both performance and user-experience.

## Key Features

* Hatch, collect and interact with Furblings
* Assign Furblings to activities/clubs to gather resources and gain experience
* Nurture and care for Furblings' needs and desires
* Connect a Furby toy via bluetooth

## Contribution

During my time at Exient, I worked within a number of areas as part of weekly sprints, including data-driven Furbling traits, the user-interface, Furbling interaction, activity assignment, object pooling and bug-fixing.