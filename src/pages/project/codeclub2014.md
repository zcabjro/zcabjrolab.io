---
title: "Code Club 2014"
date: "2014-05"
---

During 2014, I ran a Year 5/6 school club focused on making games through visual programming with Scratch. I enjoyed organising and communicating some programming essentials to enthusiastic, young learners and was amazed at the diversity of ideas that went into their games. It was great to see the pupils gradually build up enough confidence to pursue making games using ideas of their own.