---
title: "Flight Deck Simulator"
date: "2015-07"
---

Using C#, I developed an Air Marshal simulator with Unity, the Kinect 2 and Oculus Rift. I planned and implemented all required tasks, whilst regularly feeding back to my supervisor. This demanded the application of agile prinicples to ensure delivery of a working product. It proved to be very useful for getting to know Unity's revised networking model. Player actions needed to be synchronised across the network, including the body tracked skeleton of the flight deck marshal. The result was used by the academy as part of a research effort into commercial off the shelf software.

## Key Features
* Integration of Kinect 2 and Oculus Rift
* Single-player and multi-player modes, including a lobby system
* Machine learned gesture detection for flight deck instruction

## Contribution

Sole developer, implementing all functionality with accompanying documentation.