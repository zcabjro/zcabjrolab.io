---
title: "Tank Club"
date: "2016-03"
---

Working with some wonderful students and staff at the City of London School, I had the chance to build two, thematically linked games. The first, aimed at younger students with little to no programming experience, was a 2D tank arena game meant to introduce participants to Unity and some core principles of 2D game development, such as animation, collisions and scripting. The second project targeted older students with either more experience or the willingness to learn more advanced programming concepts. This took the form of a 3D tanks game, leveraging Unity's physics and particle systems alongside behaviour scripting for tank and game management.

## Key Features

* Keyboard controlled, two-player arena game
* Tanks shoot and suffer locational damage
* Tanks can capture the opposition's base
* Bases change hands whenever they are being captured simultaneously, rewarding mixed efforts of attack and capture

## Contribution

As the one responsible for tutoring the group, I planned and delivered lessons over the course of several weeks, first prototyping the iteration alone before guiding students to have them reach the same stage.