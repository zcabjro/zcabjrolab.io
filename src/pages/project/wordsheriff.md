---
title: "Word Sheriff"
date: "2016-08"
---

In connection with UCL research into Natural Language Processing, Word Sheriff was developed as a Game With A Purpose (GWAP) for generating linguistic annotations for use with machine learning and statistical approaches to solving NLP problems. By disguising the annotation task as a game, we circumvent the need for cost-bound linguistic expertise, and present a long-term data source that is fun to play! The generated datasets were used to evaluate existing NLP methods.

The project report can be found [here](https://drive.google.com/file/d/0B3wOc_-0Tn1TTlgtamZiakpYSjg/view).

Additionally, Word Sheriff was used by research into novel ways to evaluate AIs that communicate using natural language, winning a [Facebook AI Research award](http://www.cs.ucl.ac.uk/news/article/team_of_ucl_undergrads_win_facebook_ai_research_award/). The publication can be found here: [Defining Words with Words: Beyond the Distributional Hypothesis. In Proceedings of the 1st Workshop on Evaluating Vector-Space Representations for
NLP, Berlin, German y, August 2016. Association for Computational Linguistics](http://www.aclweb.org/anthology/W/W16/W16-2522.pdf).