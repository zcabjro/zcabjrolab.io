---
title: "CTSNet Article Finder"
date: "2014-04"
youtube: "https://www.youtube-nocookie.com/embed/e_aF6a08V0Y?rel=0&amp;showinfo=0"
---

Built for clients from CTSNet, the leading online source of information about cardiothoracic surgery, the application integrates with PubMed's online article repository for the search and display of a wide variety of articles. Using cordova, the application runs across all major mobile platforms.

## Key Features

* Journal filtered, keyword search of PubMed articles
* Access current journal issues using RSS
* Local storage of recently read and favourite articles

## Contribution

As part of a three person team, I was responsible for much of the research and business logic for article search and current issue retrieval. This involved programming in JavaScript and working alongside the app's UI designer. I also had the pleasure of liaising directly with the clients, which proved to be a valuable lesson in effective client elicitation.