---
title: "Freshly"
date: "2015-10"
youtube: "https://www.youtube-nocookie.com/embed/_Dks7_pBiIA?rel=0&amp;showinfo=0"
---

Freshly is an application for monitoring, grading, and analysing an individual's daily and historical exposure to various pollutants, thus empowering users to make health-conscious and informed decisions. As my first native Android application, it presented an excellent learning experience, especially for working in a larger team comprised of many different roles. Clients from the London Sustainability Exchange went on to adopt the codebase following the app's completion.

## Key Features

* Analyse location data over the course of a day
* Integrate with London air quality data
* Grade the user's exposure to air pollutants
* Visualise exposure over time

## Contribtution

As a co-leading member of the front-end team, I was responsible for the delivery of the android client application. This included the design and implementation of the user-interface as well as client-side business logic for retrieving locations, managing the user session, storing and querying score and location data and for interacting with the backend API to exchange location data and retrieve daily and monthly scores for the logged in user.