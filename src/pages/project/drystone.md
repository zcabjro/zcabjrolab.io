---
title: "Dry Stone VR"
date: "2016-10"
youtube: "https://www.youtube-nocookie.com/embed/YtqWwkRyw8A?rel=0&amp;showinfo=0""
---

Intended to provide training, the game centres around the construction of dry stone walls using the HTC Vive. Such walls are built using irregularly-shaped stones without mortar, adding an additional element of difficulty when it comes to maintaining a stable structure. Ordinarily, dry stone wall construction needs to happen on-site, with hundreds of stones at one's disposal. Within VR, we can take shortcuts to streamline the process and focus on the training.

## Key Features

* Build a wall from procedurally generated stones
* In-game supervisor demonstrates the task and gives advice about how to improve
* Use voice commands to skip layers and undo/redo actions
* Developed in Unity using SteamVR and VRTK

## Contribution

As part of a two person team, I was responsible for user interaction (Vive controllers and voice commands), stone sorting, environment design, instructor behaviour such as state management and flight pathing, as well as bug fixing.

## Platforms

* Windows, Virtual Reality