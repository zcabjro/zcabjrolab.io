import React from 'react'
import Flex from '../components/flex.js'
import Youtube from '../components/youtube.js'

export default ({ data }) => {
  const post = data.markdownRemark
  return (
    <Flex wrap>
      <h1>{post.frontmatter.title}</h1>
      {post.frontmatter.youtube ? <Youtube src={post.frontmatter.youtube}/> : null}
      <div dangerouslySetInnerHTML={{ __html: post.html }} />
    </Flex>
  )
}

export const query = graphql`
  query Project($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      frontmatter {
        title
        youtube
      }
    }
  }
`