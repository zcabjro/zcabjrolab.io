import React from 'react'
import Flex from '../components/flex.js'

export default ({ data }) => {
  const md = data.markdownRemark
  return (
    <Flex row wrap>
      <Flex xs={12}>
        <h1>{md.frontmatter.title}</h1>
      </Flex>
      <Flex>
        <div dangerouslySetInnerHTML={{ __html: md.html }} />
      </Flex>
    </Flex>
  )
}

export const query = graphql`
  query About($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      frontmatter {
        title
      }
    }
  }
`